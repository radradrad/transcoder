FROM golang:1.20.4-alpine AS build
WORKDIR /go/src/rad
COPY . .
RUN apk add  --no-cache ffmpeg
RUN apk update && apk add bash
RUN CGO_ENABLED=0 go build -o /go/bin/rad ./cmd/server/main.go
RUN mkdir -p streams
RUN mkdir -p tracks
RUN mkdir -p inputs

ENTRYPOINT ["/go/bin/rad"]
module gitlab.com/zantwich/rad/transcoder

go 1.20

require (
	github.com/u2takey/ffmpeg-go v0.4.1
	github.com/zackradisic/soundcloud-api v0.1.8
	google.golang.org/grpc v1.55.0
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/aws/aws-sdk-go v1.44.267 // indirect
	github.com/confluentinc/confluent-kafka-go v1.9.2 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/grafov/m3u8 v0.12.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/segmentio/kafka-go v0.4.40 // indirect
	github.com/twitchtv/twirp v8.1.3+incompatible // indirect
	github.com/u2takey/go-utils v0.3.1 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1 // indirect
)

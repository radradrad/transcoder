package utils

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func DownloadFile(url string, fileName string) error {
	file, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println("Error creating file while downloading", err)
		return err
	}
	res, err := http.Get(url)
	if err != nil {
		log.Println("Error parsing URL", err)
		return err
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Println("Error reading body", err)
		return err
	}
	file.Write(body)
	return nil
}

func AppendToFile(inputName, body string) error {
	log.Println("appending to file", inputName)
	file, err := os.OpenFile(inputName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		// Handle error.
		fmt.Println("error when opening file:", err)
		return err
	}

	// Ensure the file gets closed after writing.
	defer file.Close()

	// Write each array element to the file.
	_, err = file.WriteString(fmt.Sprintf("%s\n", body))
	if err != nil {
		fmt.Println("error when writing file:", err)
		return err
	}

	// Sync writes the file's in-memory copy to the disk.
	err = file.Sync()
	if err != nil {
		fmt.Println("error when syncing file:", err)
		return err
	}
	fmt.Println("File written successfully")

	return nil
}

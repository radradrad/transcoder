package consumer

import (
	"context"
	"encoding/json"
	"log"
	"os"

	"github.com/segmentio/kafka-go"
	"gitlab.com/zantwich/rad/transcoder/internal/core"
	"gitlab.com/zantwich/rad/transcoder/internal/transcoder"
)

type Consumer struct {
	t *transcoder.Transcoder
	r *kafka.Reader
}

func NewConsumer(t *transcoder.Transcoder) (*Consumer, error) {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{os.Getenv("KAFKA_URL")},
		GroupID:  "1",
		Topic:    os.Getenv("KAFKA_TOPIC"),
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
	return &Consumer{t, r}, nil
}

func (c *Consumer) ListenAndConsume() {
	defer func() {
		if err := c.r.Close(); err != nil {
			log.Println("Failed to close reader:", err)
		}
	}()

	for {
		m, err := c.r.ReadMessage(context.Background())
		if err != nil {
			log.Println(err)
			continue
		}

		log.Printf("message at topic/partition/offset %v/%v/%v: %s\n",
			m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

		switch string(m.Key) {
		case "download_track":
			var trackMessage core.DownloadTrackMessage
			if err := json.Unmarshal([]byte(m.Value), &trackMessage); err != nil {
				log.Println("error while unmarshaling: ", err)
				continue
			}
			log.Println("Processing")
			go c.t.DownloadTrack(trackMessage)
		case "start_livestream":
			var livestreamMessage core.StartLivestreamMessage
			if err := json.Unmarshal([]byte(m.Value), &livestreamMessage); err != nil {
				log.Println("error while unmarshaling: ", err)
				continue
			}
			log.Println("Processing")
			go c.t.Transcode(&livestreamMessage)
		default:
			log.Println("message not supported sent %s", string(m.Key))
		}
	}
}

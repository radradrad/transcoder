package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type Client struct {
	baseUrl string
}

type StatusPayload struct {
	Status string `json:"status"`
}

func NewAPI(baseUrl string) *Client {
	return &Client{
		baseUrl: baseUrl,
	}
}

func (c *Client) UpdateLivestreamStatus(livestreamId int, status string) error {
	url := fmt.Sprintf("%s/livestreams/%v/status", c.baseUrl, livestreamId)

	payload := StatusPayload{
		Status: status,
	}
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	resp, err := http.NewRequest("PATCH", url, bytes.NewBuffer(jsonPayload))
	if err != nil {
		return err
	}
	resp.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	_, err = client.Do(resp)
	if err != nil {
		return err
	}

	return nil
}

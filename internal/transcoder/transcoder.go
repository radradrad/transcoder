package transcoder

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
	"time"

	ffmpeg "github.com/u2takey/ffmpeg-go"
	soundcloudapi "github.com/zackradisic/soundcloud-api"
	"gitlab.com/zantwich/rad/transcoder/internal/client"
	"gitlab.com/zantwich/rad/transcoder/internal/core"
	"gitlab.com/zantwich/rad/transcoder/internal/utils"
)

const baseLocal = "http://localhost:4000"
const baseRemote = "https://cf-hls-media.sndcdn.com"

func (t *Transcoder) DownloadTrack(m core.DownloadTrackMessage) error {
	dir, err := os.Getwd()
	if err != nil {
		log.Println("error getting dir:", err)
		return err
	}

	log.Println("Getting URL for track")
	url, err := t.GetURL(m.Url)
	log.Println(url)
	if err != nil {
		log.Println("error while downloading url", err)
		return err
	}

	fileName := fmt.Sprintf("%s/tracks/%v_%v.mp3", dir, m.LivestreamId, m.TrackId)
	err = utils.DownloadFile(url, fileName)
	if err != nil {
		log.Println("error creating file while downloading", err)
		return err
	}

	inputFileName := fmt.Sprintf("%s/inputs/input_%v.txt", dir, m.LivestreamId)
	appendBody := fmt.Sprintf("file %s", fileName)
	utils.AppendToFile(inputFileName, appendBody)

	return nil
}

type Transcoder struct {
	urlValues url.Values
}

func (t *Transcoder) swapDomainWithParams(base string, s string) (string, error) {
	// Parse URL
	u, err := url.Parse(s)
	if err != nil {
		return "", err
	}

	// Merge baseLocal with Path
	local, err := url.Parse(base + u.Path)

	// Build local query
	localQ := local.Query()
	if err != nil {
		return "", err
	}

	for k, v := range t.urlValues {
		localQ.Add(k, v[0])
	}
	local.RawQuery = localQ.Encode()
	return local.String(), nil

}

func swapDomain(base string, s string) (string, error) {
	// Parse URL
	u, err := url.Parse(s)
	if err != nil {
		return "", err
	}

	// Merge baseLocal with Path
	local, err := url.Parse(base + u.Path)
	if err != nil {
		return "", err
	}

	// Build local query
	localQ := local.Query()
	local.RawQuery = localQ.Encode()
	return local.String(), nil
}

func (t *Transcoder) updateParamsWithUrl(s string) {
	u, err := url.Parse(s)
	if err != nil {
		log.Println("Error parsing url", err)
	}

	q := u.Query()
	t.urlValues = q
}

func (t *Transcoder) GetURL(url string) (string, error) {
	sc, err := soundcloudapi.New(soundcloudapi.APIOptions{})
	if err != nil {
		log.Println(err.Error())
	}

	s, err := sc.GetDownloadURL(url, "progressive")
	log.Println(s)
	if err != nil {
		log.Println(err)
		return "", err
	}

	// Update parmas
	t.updateParamsWithUrl(s)

	// inStr, err := swapDomain(baseLocal, s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func (t *Transcoder) Transcode(tm *core.StartLivestreamMessage) {
	// urls := strings.Split(tm.Url, "|")
	// urlWithProxy := make([]string, len(urls))
	// for i, url := range urls {
	// 	newUrl, err := t.GetURL(url)
	// 	if err != nil {
	// 		break
	// 	}
	// 	urlWithProxy[i] = newUrl
	// }

	client := client.NewAPI(os.Getenv("LIVESTREAMS_API_URL"))

	// TODO: Structured logs
	err := client.UpdateLivestreamStatus(tm.LivestreamId, "STREAMING")
	if err != nil {
		log.Println("err while updating track status", err)
	}
	fmt.Println("Status updated to STREAMING on track", "livestream", tm.LivestreamId)
	// transcode
	outStr := fmt.Sprintf("./stream/%v.m3u8", tm.LivestreamId)
	// inStr := strings.Join(urlWithProxy, "|")
	inStr := fmt.Sprintf("./stream/input_%v.txt", tm.LivestreamId)

	t.execFfmpeg(inStr, outStr)
	// go client.UpdateLivestreamStatus(tm.LivestreamId, "CLOSED")
	if err != nil {
		log.Println("err while updating track status", err)
	}
	fmt.Println("Status updated to CLOSED on track", "livestream", tm.LivestreamId)
}

func NewTranscoder() (*Transcoder, error) {
	t := Transcoder{}

	// Start proxy
	go t.startProxy()

	return &t, nil
}

func getOffset(off string) string {
	if off == "" {
		return "00:00:00"
	} else {
		return off
	}
}

func (t *Transcoder) execFfmpeg(inFileName, outFileName string) {
	fmt.Printf("Exec Ffmpeg with %s to %s", inFileName, outFileName)
	err := ffmpeg.
		Input("./inputs/input_7.txt", ffmpeg.KwArgs{"f": "concat", "safe": "0", "re": ""}).
		Output("./stream/7.m3u8", ffmpeg.KwArgs{"hls_time": "10", "hls_list_size": "5", "hls_flags": "omit_endlist"}).
		OverWriteOutput().
		Run()
	if err != nil {
		log.Println("Error running ffmpeg", err.Error())
	}
}

func genTempSocket() string {
	rand.Seed(time.Now().Unix())
	socket := path.Join(os.TempDir(), fmt.Sprintf("%d_sock", rand.Int()))
	l, err := net.Listen("unix", socket)
	if err != nil {
		log.Println("Error allocating socket", err)
	}
	go printSocket(l)
	return socket
}

func printSocket(l net.Listener) {
	fd, err := l.Accept()
	if err != nil {
		log.Println("Accept error:", err)
	}
	buf := make([]byte, 16)
	for {
		_, err := fd.Read(buf)
		if err != nil {
			fmt.Println("Error during printing", err.Error())
			l.Close()
			break
		}
		fmt.Printf("%s", buf)
	}
}

func addHeaders(h http.Handler) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		h.ServeHTTP(w, r)
	}
}

func (t *Transcoder) startProxy() {
	http.HandleFunc("/stream/", addHeaders(http.FileServer(http.Dir("./"))))
	// http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	w.Write([]byte("ok"))
	// })

	// Proxy the request without any headers :P
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		t.GetURL("https://soundcloud.com/llwll/no-longer-human")
		s, err := t.swapDomainWithParams(baseRemote, r.URL.String())
		log.Println(s)
		fmt.Println(s)
		if err != nil {
			log.Println(err)
		}
		res, err := http.Get(s)
		if err != nil {
			log.Println("Error parsing URL", err)
		}
		body, err := io.ReadAll(res.Body)
		if err != nil {
			log.Println("Error reading body", err)
		}
		// if strings.Contains(string(body), "Forbidden") {
		s, err = soundcloudapi.FetchClientID()
		if err != nil {
			log.Println("lol")
		}
		log.Println(s)
		// }
		w.Header().Set("content-type", "audio/mpegurl")
		w.Write(body)
	})

	fmt.Println("Proxy and file server listening at 4000")
	http.ListenAndServe(":4000", nil)
}

func (t *Transcoder) CheckClient() {

}

func (t *Transcoder) DownloadAndModifyPlaylist(url string, outputFileName string) error {
	log.Println("Downlaoding and sanitising playlist")
	res, err := http.Get(url)
	if err != nil {
		log.Println("Error parsing URL", err)
		return err
	}

	// Open the output file for writing
	outputFile, err := os.Create(outputFileName)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	scanner := bufio.NewScanner(res.Body)
	writer := bufio.NewWriter(outputFile)

	var shouldSwapNextLine bool

	for scanner.Scan() {
		line := scanner.Text()

		if shouldSwapNextLine {
			// line = line
			log.Println("line")
			if err != nil {
				return err
			}
			shouldSwapNextLine = false
		} else if strings.HasPrefix(line, "#EXTINF") {
			log.Println("found")
			shouldSwapNextLine = true
		}

		_, err := writer.WriteString(line + "\n")
		if err != nil {
			return err
		}
	}

	if scanner.Err() != nil {
		// Handle error
		return scanner.Err()
	}

	return writer.Flush()
}

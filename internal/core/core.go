package core

type StartLivestreamMessage struct {
	LivestreamId int `json:"livestreamId"`
}

type DownloadTrackMessage struct {
	LivestreamId int    `json:"livestreamId"`
	TrackId      int    `json:"trackId"`
	Url          string `json:"url"`
}

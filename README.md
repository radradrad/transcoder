## rad

rad is a highly available, eventually consistent replicated service that allows you to serve (HLS) and livestream soundcloud URLs

### first steps
- download soundcloud song in hls
- ffmpeg command `ffmpeg -re -i audio/outputlist.m3u8 -c:a aac -b:a 128k -hls_time 4 -hls_list_size 5 -hls_wrap 10 -hls_flags delete_segments stream/output.m3u8`

## TODO
- [ ] separate proxy from file server
- [ ] encapsulate stuff
- [ ] add tests :)
- [ ] add managed distributed fs
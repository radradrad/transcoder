package main

import (
	"fmt"
	"log"

	"gitlab.com/zantwich/rad/transcoder/internal/consumer"
	"gitlab.com/zantwich/rad/transcoder/internal/transcoder"
)

func main() {
	trsc, err := transcoder.NewTranscoder()
	if err != nil {
		log.Println(err)
	}

	c, err := consumer.NewConsumer(trsc)
	if err != nil {
		log.Println(err)
	}

	fmt.Println("Listening to rad.livestreams")
	c.ListenAndConsume()
}
